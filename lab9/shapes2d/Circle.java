package shapes2d;

public class Circle extends Object{
    public int radius;

    public Circle(double radius){
        this.radius = radius;
    }

    public double area(){
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius + ", area=" + area() +
                '}';
    }
}
